﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calculator
{
    public class Calculator
    {

        public int Value1 { get; set; }
        public int Value2 { get; set; }
        public string Operation { get; set; }

        public Calculator (int val1, int val2, char operation)
        {

        }

        //public int validateNumber()
        //{
      
        //}

        public Calculator()
        {
            Console.WriteLine("You have created an instance of the calculator class.");
        }

        public static int PerformCalc(Calculator calc)
        {
            int answer = 0;

            switch (calc.Operation)
            {
                case "+" :
                    answer = calc.Add();
                    break;
                case "-":
                    answer = calc.Subtract();
                    break;
                case "*" :
                    answer = calc.Multiply();
                    break;
                case "/" :
                    answer = calc.Divide();
                    break;
                default:
                    Console.WriteLine("Invalid operation provided");
                    break; 
            }
            return answer;
        }

        public static bool ValidateOperator(string operation)
        {
            if (operation == "+" || operation == "-" || operation == "*" || operation == "/")
            {
                Console.WriteLine("Operation selected: " + operation);
                return true;
            }
            else
            {
                Console.WriteLine("Invalid operation.");
                return false;
            }

        }

        public static void RetrieveOperation(Calculator calc)
        {
            Console.WriteLine("Enter an operation (+ - * /)");
            string operation = Convert.ToString(Console.ReadLine());
            bool validOperator = ValidateOperator(operation);

            if (validOperator)
            {
                calc.Operation = Convert.ToString(operation);
            }
            else { RetrieveOperation(calc); }

        }

        public static void RetrieveFirstNumber(Calculator calc)
        {
            int userValue1;
            try
            {
                Console.WriteLine("Enter First value");
                userValue1 = Convert.ToInt32(Console.ReadLine());
                Console.WriteLine("First Value entered: " + userValue1);
                calc.Value1 = userValue1;
            }
            catch
            {
                Console.WriteLine("Invalid entry.");
                RetrieveFirstNumber(calc);
            }
        }

        public static void RetrieveSecondNumber(Calculator calc)
        {
            int userValue2;
            try
            {
                Console.WriteLine("Enter second value");
                userValue2 = Convert.ToInt32(Console.ReadLine());
                Console.WriteLine("Second Value entered: " + userValue2);
                calc.Value2 = userValue2;
            }
            catch
            {
                Console.WriteLine("Invalid entry.");
                RetrieveSecondNumber(calc);
            }
        }

        public int Add()
        {
            return this.Value1 + this.Value2;
        }

        public int Subtract()
        {
            return this.Value1 - this.Value2;
        }

        public int Multiply()
        {
            return this.Value1 * this.Value2;
        }

        public int Divide()
        {
            return this.Value1 / this.Value2;
        }
    }
}
