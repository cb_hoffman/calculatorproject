﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calculator
{
    class Program
    {
        static void Main(string[] args)
        {
            Calculator firstCalc = new Calculator();

            Calculator.RetrieveFirstNumber(firstCalc);
            Calculator.RetrieveOperation(firstCalc);
            Calculator.RetrieveSecondNumber(firstCalc);

            int solution = Calculator.PerformCalc(firstCalc);

            Console.WriteLine("Answer : " + solution);

        }
    }
}
